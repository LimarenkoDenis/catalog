export class Developer {
  id: number;
  firstName: string;
  lastName: string;
  sex: string;
  email: string;
  phone: string;
  birth: string;
  photo: string;
  title: string;
  skiils: [
    string
  ];
}
