import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Developer } from './../developer';
import { DevelopersService } from './../developers.service';

@Component({
  selector: 'developer-detail',
  templateUrl: 'developer-detail.component.html'
})
export class DeveloperDetailComponent implements OnInit {
  developer: Developer;
  error: any;

  constructor(
    private DevelopersService: DevelopersService,
    private route: ActivatedRoute) {
  }

  detail (id:number) {
    this.DevelopersService.detail(id)
      .then(developer => this.developer = developer)
      .catch(error => this.error = error);;
  }

  save (developer: Developer) {
    this.DevelopersService.save(developer)
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
        this.detail(id)
    });
  }

  goBack() {
    window.history.back();
  }
}
