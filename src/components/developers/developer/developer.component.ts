import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Developer } from './../developer';
import { DevelopersService } from './../developers.service';

@Component({
  selector: 'developers',
  templateUrl: 'developers.component.html',
})
export class DevelopersComponent implements OnInit {
  developers: Developer[];
  selectedDev: Developer;
  error: any;

  constructor(private router: Router, private DevelopersService: DevelopersService) { }

  list() {
    this.DevelopersService.list()
      .then(developers => this.developers = developers)
      .catch(error => this.error = error);
  }

  ngOnInit() {
    this.list();
  }

  onSelect(developer: Developer) {
    this.selectedDev = developer;
  }

  gotoDetail() {
    this.router.navigate(['/detail', this.selectedDev.id]);
  }
}
