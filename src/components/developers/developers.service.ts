import { Developer } from './developer';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class DevelopersService {
  constructor(private http: Http) { }

  list() {
    return this.http.get(`${process.env.API}/developers`)
      .toPromise()
      .then(res => res.json() as Developer[])
      .catch(this.handleError);
  }

  create(developerData: any) {
    return this.http.post(`${process.env.API}/developers`, developerData)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  update(developer: any) {
    return this.http.put(`${process.env.API}/developers/${developer.id}`, developer)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  detail(id: number) {
    return this.http.get(`${process.env.API}/developers/${id}`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  save(developer: any) {
    if (developer.id) return this.update(developer);
    return this.create(developer);
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
