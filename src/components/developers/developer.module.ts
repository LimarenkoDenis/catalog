import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }     from '@angular/http';
import { MdInputModule } from '@angular2-material/input/input';
import { MdButtonModule } from '@angular2-material/button/button';
import { MdListModule } from '@angular2-material/list/list';
import { MdCardModule } from '@angular2-material/card/card';

import { DevelopersComponent }      from './developer/developer.component';
import { DeveloperDetailComponent } from './developer-detail/developer-detail.component';
import { DevelopersService }        from './developers.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdInputModule,
    MdButtonModule,
    MdListModule,
    MdCardModule
  ],
  declarations: [
    DevelopersComponent,
    DeveloperDetailComponent
  ],
  providers: [
    DevelopersService
  ],
  exports: [DevelopersComponent, DeveloperDetailComponent]
})
export class DeveloperModule {}
