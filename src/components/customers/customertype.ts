export class Customer  {
  id: number;
  firstName: string;
  lastName: string;
  country: string;
  timeZone: number;
  sex: string;
  email: string;
  phone: string;
  birth: string;
  photo: string;
}
