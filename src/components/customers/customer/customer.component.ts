import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from './../customertype';

@Component({
  selector: 'customer',
  templateUrl: 'customer.html',
})

export class CustomerComponent implements OnInit {
  @Input()
  customers: Customer;

  constructor(private router: Router) { }
  ngOnInit() { }

  gotoDetail(id: number) {
    this.router.navigate(['/customers', id]);
  }
}
