import { Component } from '@angular/core';
import { CustomersService } from '../customers.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Customer } from './../customertype';

@Component({
  selector: 'customer-detail',
  templateUrl: 'customer-detail.html',
  providers: [CustomersService]
})

export class CustomerDetailComponent {
  customer: Customer;
  error: any;
  private sub: any;

  constructor(private CustomersService: CustomersService, private route: ActivatedRoute) { }

  detail(id: string) {
    this.CustomersService.detail(id)
      .then(customer => {
        this.customer = customer
      })
      .catch(error => this.error = error);
  }

  save (customer: Customer) {
    this.CustomersService.save(customer)
  }

  update(customer: Customer) {
    this.CustomersService.update(customer)
      .then(customer => {
        this.customer = customer
      })
      .catch(error => this.error = error);
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = params['id'];
        this.detail(id)
    });
  }
}
