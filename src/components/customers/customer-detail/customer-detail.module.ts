import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { CustomerDetailComponent }  from './customer-detail.component';
import { CustomersService } from '../customers.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule
  ],
  declarations: [CustomerDetailComponent],
  providers: [CustomersService],
  exports: [CustomerDetailComponent]
})
export class CustomerDetailModule { }
