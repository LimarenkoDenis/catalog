import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { CustomersListComponent }  from './customers-list.component';
import { CustomersService } from '../customers.service';

@NgModule({
  imports: [BrowserModule, FormsModule],
  declarations: [CustomersListComponent],
  providers: [CustomersService],
  exports: [CustomersListComponent]
})
export class CustomersListModule { }
