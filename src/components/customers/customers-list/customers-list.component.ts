import { Component } from '@angular/core';
import { CustomersService } from '../customers.service';
import { CustomerComponent } from './../customer';
import { Customer } from './../customertype'

@Component({
  selector: 'customers-list',
  templateUrl: 'customers-list.html',
  providers: [CustomersService],
  directives: [CustomerComponent]
})

export class CustomersListComponent {
  customers: Customer[];
  count: number;
  error: any;

  constructor(private CustomersService: CustomersService) { }

  list() {
    this.CustomersService.list()
      .then(customers => {
        this.count = customers.length;
        this.customers = customers
      })
      .catch(error => this.error = error);
  }

  remove(id: string) {
    if (confirm('delete?')) {
      this.CustomersService.remove(id)
        .then(() => {
          this.ngOnInit()
        })
        .catch(error => this.error = error);
    }
  }

  ngOnInit() {
    this.list();
  }

}
