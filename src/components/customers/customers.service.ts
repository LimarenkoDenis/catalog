/* global process.env.API*/

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class CustomersService {
  constructor(private http: Http) { }

  list() {
    return this.http.get(`${process.env.API}/customers`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  create(customerData: any) {
    return this.http.post(`${process.env.API}/customers`, customerData)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  update(customer: any) {
    return this.http.put(`${process.env.API}/customers/${customer.id}`, customer)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  detail(id: string) {
    return this.http.get(`${process.env.API}/customers/${id}`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  remove(id: string) {
    return this.http.delete(`${process.env.API}/customers/${id}`)
      .toPromise()
      .catch(this.handleError);
  }

  save(customer: any) {
    if (customer.id) return this.update(customer);
    return this.create(customer);
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
