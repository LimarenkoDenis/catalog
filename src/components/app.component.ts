import { Component } from '@angular/core';
import './../public/css/styles.css';

@Component({
  selector: 'app',
  templateUrl: './app.html'
})

export class AppComponent {
  title = 'Catalog';
}
