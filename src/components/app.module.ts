import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { HttpModule }     from '@angular/http';

import { AppComponent }   from './app.component';
import { routing }        from './app.routing';

import { DevelopersService }        from './developers/developers.service';
import { DeveloperModule } from './developers/developer.module';

import {
  CustomersListModule,
  CustomerDetailModule,
  CustomersService
} from './customers';


@NgModule({
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    CustomersListModule,
    CustomerDetailModule,
    DeveloperModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    DevelopersService,
    CustomersService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
